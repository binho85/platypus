<?php

namespace Platypus\Http;

class Response {
    public static function json($data) {
        header("Content-Type: application/json");
        echo json_encode($data, JSON_HEX_QUOT|JSON_HEX_APOS);
    }
}